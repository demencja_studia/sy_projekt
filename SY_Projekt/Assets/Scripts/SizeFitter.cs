﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeFitter : MonoBehaviour
{
    [Range(0,1)]
    public float heightInPrecent;
    [Range(0, 1)]
    public float widthInPrecent;
    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        float newHeight = Screen.height * heightInPrecent;
        float newWidth = Screen.width * widthInPrecent;
        rectTransform.sizeDelta = new Vector2(newWidth, newHeight);
    }
}
