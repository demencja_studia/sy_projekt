﻿using SymulacjaProjektPKKT;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using static SymulacjaProjektPKKT.ObliczanieMacierzy;

public class MainScript : MonoBehaviour
{
    private List<InputData> inputData;
    private MyMatrix myMatrix;
    public GameObject ElementPrefab;
    public Transform ElementContent;
    public Transform MatrixG;
    public Transform MatrixI;
    public Transform MatrixV;
    private ObliczanieMacierzy obliczanie;
    public CreatePola polaScript;
    public Transform[] delete;
    public Text wynikV;
    public Text wynikI;
    public Text wynikIr;
    private List<InputData> wszystkieRezystory = new List<InputData>();

    // Use this for initialization
    void Start()
    {
        inputData = new List<InputData>();
        myMatrix = new MyMatrix(0, polaScript);
        ActiveSlider(false);
    }

    Wynik wynik;
    public void ButtonOblicz()
    {
        wynikV.text = "";
        wynikI.text = "";
        wynikIr.text = "";

        float[,] matrixG;
        float[] matrixI;

        myMatrix.Utnij(out matrixG, out matrixI);
        obliczanie = new ObliczanieMacierzy(matrixI, matrixG);
        wynik = obliczanie.Oblicz();
        for (int i = 0; i < wynik.V.Length; i++)
        {
            wynikV.text += myMatrix.matrixV[i + 1] + " = " + Math.Round(wynik.V[i], 2) + "\n";
        }
        var I = wynik.WszystkieMacierze.LastOrDefault().I;
        for (int i = 0; i < I.Length; i++)
        {
            wynikI.text += "I" + (i + 1) + " = " + Math.Round(I[i], 2) + "\n";
        }
        foreach (InputData data in wszystkieRezystory)
        {
            wynikIr.text += ObliczIr(data, wynik.V) + "\n";
        }
        ActiveSlider(true);
    }

    private string ObliczIr(InputData data, float[] V)
    {
        if (V == null)
        {
            return "V = null";
        }
        string name = data.name;
        float[] newV = new float[V.Length + 1];
        newV[0] = 0;
        for (int i = 1; i < newV.Length; i++)
        {
            newV[i] = V[i - 1];
        }
        if (newV.Length < data.node1)
        {
            return "V za małe+";
        }
        if (newV.Length < data.node2)
        {
            return "V za małe-";
        }
        float V1 = newV[data.node1];
        float V2 = newV[data.node2];
        float G = (1 / data.value);
        var value = Math.Round((V1 - V2) * G, 2);
        return name + " = " + value;
    }

    public void AddInputData(InputData data)
    {
        Debug.Log("Dodano");
        inputData.Add(data);
        var obj = Instantiate(ElementPrefab, ElementContent);
        InputElementUI inputElementUI = obj.GetComponent<InputElementUI>();
        if (inputElementUI != null)
        {
            inputElementUI.Uzupelnij(data);
            myMatrix.AddData(data);
            polaScript.Create(myMatrix);
            if (data.type == ElementTpe.R)
            {
                wszystkieRezystory.Add(data);
            }
        }
    }

    public Slider slider;
    public void OnSliderMove()
    {
        int sliderValue = Mathf.RoundToInt(slider.value);
        if (sliderValue == -1)
        {
            polaScript.Create(myMatrix);
        }
        else
        {
            polaScript.Create(wynik.WszystkieMacierze[sliderValue], myMatrix.matrixV);
        }
    }

    public void ActiveSlider(bool value)
    {
        slider.gameObject.SetActive(value);
        if (value == true)
        {
            int iloscPrzeksztalcen = wynik.WszystkieMacierze.Count;
            slider.minValue = -1;
            slider.maxValue = iloscPrzeksztalcen - 1;
            slider.value = -1;
        }
    }

    public void ResteAll()
    {
        polaScript.ResetMatrix();
        inputData.Clear();
        ElementContent.Clear();
        wynikV.text = "";
        wynikI.text = "";
        wynikIr.text = "";
        foreach (Transform parent in delete)
        {
            foreach (Transform child in parent)
            {
                Destroy(child.gameObject);
            }
        }
        myMatrix = new MyMatrix(0, polaScript);
        wynik = null;
        ActiveSlider(false);
        wszystkieRezystory.Clear();
    }
}

