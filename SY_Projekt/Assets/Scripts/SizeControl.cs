﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeControl : MonoBehaviour
{
    public float sizeInPrecent;
    public RectTransform toScale;

    private void Start()
    {
        float size = Screen.width * sizeInPrecent;
        if(toScale != null)
        {

            toScale.sizeDelta = new Vector2(size, size);
            toScale.offsetMin = new Vector2(toScale.offsetMin.x, 0);
            toScale.offsetMax = new Vector2(toScale.offsetMax.x, 0);

        }
    }
}
