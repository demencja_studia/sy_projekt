﻿using System;
using System.Collections;
using System.Collections.Generic;
using SymulacjaProjektPKKT;
using UnityEngine;
using UnityEngine.UI;

public class CreatePola : MonoBehaviour
{
    public GameObject polaPrefab;
    public float elementSizeInPrecentMax;
    //public Vector2 matrixSize;
    public Vector2 startPointPrecent;
    public int elementOffset;
    private Vector2 starPoint;
    private float maxLength;
    private float elementWidth;
    public GameObject polaV;
    public GameObject polaI;

    public void ResetMatrix()
    {
        foreach (Transform tran in transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaV.transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaI.transform)
        {
            Destroy(tran.gameObject);
        }
    }

    public void Create(int size)
    {
        foreach (Transform tran in transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaV.transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaI.transform)
        {
            Destroy(tran.gameObject);
        }
        maxLength = Screen.width * elementSizeInPrecentMax - Screen.width * startPointPrecent.x;

        starPoint = new Vector2(Screen.width * startPointPrecent.x, Screen.height * startPointPrecent.y);
        CreateMatrix(new Vector2(size, size));
    }

    public float offsetV;
    public float offsetI;
    private void CreateMatrix(Vector2 size)
    {
        elementWidth = (maxLength - elementOffset * (size.x - 2)) / (size.x);
        starPoint += new Vector2(elementWidth / 2f, 0f);
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                GameObject obj = Instantiate(polaPrefab, gameObject.transform);
                obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
                obj.GetComponent<RectTransform>().position = starPoint + new Vector2((elementWidth + elementOffset) * x, -(elementWidth + elementOffset) * y);
                obj.name = "" + y + "" + x;
            }
        }
        for (int y = 0; y < size.y; y++)
        {
            GameObject obj = Instantiate(polaPrefab, polaV.transform);
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
            obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetV - elementWidth / 2f, -(elementWidth + elementOffset) * y);
            obj.name = "" + y;
        }
        for (int y = 0; y < size.y; y++)
        {
            GameObject obj = Instantiate(polaPrefab, polaI.transform);
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
            obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetI - elementWidth / 2f, -(elementWidth + elementOffset) * y);
            obj.name = "" + y;
        }
        print("Created!");
    }

    internal void Create(ObliczanieMacierzy.Macierze macierze, string[] matrixV)
    {
        int size = macierze.I.Length;

        foreach (Transform tran in transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaV.transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaI.transform)
        {
            Destroy(tran.gameObject);
        }
        maxLength = Screen.width * elementSizeInPrecentMax - Screen.width * startPointPrecent.x;

        starPoint = new Vector2(Screen.width * startPointPrecent.x, Screen.height * startPointPrecent.y);
        CreateMatrix(new Vector2(size, size), macierze, matrixV);
    }

    internal void Create(MyMatrix matrix)
    {
        int size = matrix.matrixI.Length;

        foreach (Transform tran in transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaV.transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in polaI.transform)
        {
            Destroy(tran.gameObject);
        }
        maxLength = Screen.width * elementSizeInPrecentMax - Screen.width * startPointPrecent.x;

        starPoint = new Vector2(Screen.width * startPointPrecent.x, Screen.height * startPointPrecent.y);
        CreateMatrix(new Vector2(size, size), matrix);
    }

    private void CreateMatrix(Vector2 size, MyMatrix matrix)
    {
        elementWidth = (maxLength - elementOffset * (size.x - 2)) / (size.x);
        starPoint += new Vector2(elementWidth / 2f, 0f);
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                GameObject obj = Instantiate(polaPrefab, gameObject.transform);
                obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
                obj.GetComponent<RectTransform>().position = starPoint + new Vector2((elementWidth + elementOffset) * x, -(elementWidth + elementOffset) * y);
                obj.name = "" + y + "" + x;
                obj.GetComponent<Text>().text = Math.Round(matrix.matrixG[y, x], 4).ToString();
            }
        }
        for (int y = 0; y < size.y; y++)
        {
            GameObject obj = Instantiate(polaPrefab, polaV.transform);
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
            obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetV - elementWidth / 2f, -(elementWidth + elementOffset) * y);
            obj.name = "" + y;
            obj.GetComponent<Text>().text = matrix.matrixV[y];
        }
        for (int y = 0; y < size.y; y++)
        {
            GameObject obj = Instantiate(polaPrefab, polaI.transform);
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
            obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetI - elementWidth / 2f, -(elementWidth + elementOffset) * y);
            obj.name = "" + y;
            obj.GetComponent<Text>().text = Math.Round(matrix.matrixI[y], 3).ToString();
        }
        print("Created!");
    }

    private void CreateMatrix(Vector2 size, ObliczanieMacierzy.Macierze macierze, string[] matrixV)
    {
        elementWidth = (maxLength - elementOffset * (size.x - 2)) / (size.x);
        starPoint += new Vector2(elementWidth / 2f, 0f);
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                GameObject obj = Instantiate(polaPrefab, gameObject.transform);
                obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
                obj.GetComponent<RectTransform>().position = starPoint + new Vector2((elementWidth + elementOffset) * x, -(elementWidth + elementOffset) * y);
                obj.name = "" + y + "" + x;
                obj.GetComponent<Text>().text = Math.Round(macierze.G[y, x], 3).ToString();
            }
        }
        for (int y = 0; y < size.y; y++)
        {
            GameObject obj = Instantiate(polaPrefab, polaV.transform);
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
            obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetV - elementWidth / 2f, -(elementWidth + elementOffset) * y);
            obj.name = "" + y;
            obj.GetComponent<Text>().text = matrixV[y + 1];
        }
        for (int y = 0; y < size.y; y++)
        {
            GameObject obj = Instantiate(polaPrefab, polaI.transform);
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
            obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetI - elementWidth / 2f, -(elementWidth + elementOffset) * y);
            obj.name = "" + y;
            obj.GetComponent<Text>().text = Math.Round(macierze.I[y], 2).ToString();
        }
        print("Created!");
    }

    //private void CreateMatrix(Vector2 size, SymulacjaProjektPKKT.ObliczanieMacierzy.Wynik wynik)
    //{
    //    elementWidth = (maxLength - elementOffset * (size.x - 2)) / (size.x);
    //    starPoint += new Vector2(elementWidth / 2f, 0f);
    //    for (int y = 0; y < size.y; y++)
    //    {
    //        for (int x = 0; x < size.x; x++)
    //        {
    //            GameObject obj = Instantiate(polaPrefab, gameObject.transform);
    //            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
    //            obj.GetComponent<RectTransform>().position = starPoint + new Vector2((elementWidth + elementOffset) * x, -(elementWidth + elementOffset) * y);
    //            obj.name = "" + y + "" + x;
    //        }
    //    }
    //    for (int y = 0; y < size.y; y++)
    //    {
    //        GameObject obj = Instantiate(polaPrefab, polaV.transform);
    //        obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
    //        obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetV - elementWidth / 2f, -(elementWidth + elementOffset) * y);
    //        obj.name = "" + y;
    //    }
    //    for (int y = 0; y < size.y; y++)
    //    {
    //        GameObject obj = Instantiate(polaPrefab, polaI.transform);
    //        obj.GetComponent<RectTransform>().sizeDelta = new Vector2(elementWidth, elementWidth);
    //        obj.GetComponent<RectTransform>().position = starPoint + new Vector2(Screen.width * offsetI - elementWidth / 2f, -(elementWidth + elementOffset) * y);
    //        obj.name = "" + y;
    //    }
    //    print("Created!");
    //}
}
