﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SymulacjaProjektPKKT
{
    class ObliczanieMacierzy
    {
        private readonly float[,] OrginalG;
        private readonly float[] OrginalI;
        private double wilkosc;
        public ObliczanieMacierzy(float[] orginalI, float[,] orginalG)
        {
            this.OrginalI = orginalI;
            this.OrginalG = orginalG;
            this.wilkosc = Math.Sqrt(orginalG.Length);
        }
        public Wynik Oblicz()
        {
            int iloscPrzeksztalcen = ZnajdzIloscPrzeksztalcen();

            return ObliczWynik(iloscPrzeksztalcen);
        }
        private Wynik ObliczWynik(int iloscPrzeksztalcen)
        {
            List<Macierze> wszystkieMacierze = new List<Macierze>();
            wszystkieMacierze.Add(new Macierze(KopiujG(this.OrginalG), this.OrginalI, 0));

            for (int przeksztalcenie = iloscPrzeksztalcen; przeksztalcenie < this.wilkosc; przeksztalcenie++)
            {
                var macierzPoprzednia = wszystkieMacierze[przeksztalcenie - 1];
                var macierz = KopiujG(macierzPoprzednia.G);
                var noweI = KopiujI(macierzPoprzednia.I);

                wszystkieMacierze.Add(new Macierze(macierz, noweI, przeksztalcenie));
                for (int i = przeksztalcenie; i < this.wilkosc; i++)
                {
                    for (int j = przeksztalcenie - 1; j < this.wilkosc; j++)
                    {
                        macierz[i, j] = ObliczPrzeksztalcenie(i, j, przeksztalcenie, macierzPoprzednia.G);
                        //Console.WriteLine($"Modyufikcaja {przeksztalcenie}: {macierz[i, j]}");
                    }
                    noweI[i] = ObliczNoweI(i, przeksztalcenie, macierzPoprzednia.I, macierzPoprzednia.G);
                    //Console.WriteLine($"I {przeksztalcenie}: {noweI[i]}");
                }

            }
            return new Wynik(wszystkieMacierze, ObliczV(wszystkieMacierze.OrderBy(p => p.Id).LastOrDefault()));
        }
        private float[] ObliczV(Macierze ostatniaMacierz)
        {
            if (ostatniaMacierz == null)
                throw new Exception("Nie obliczono wszystkich macierzy !");

            float[] wynik = new float[ostatniaMacierz.Id + 1];
            for (int i = ostatniaMacierz.Id; i >= 0; i--)
            {
                if (i == ostatniaMacierz.Id)
                    wynik[i] = WyliczWynikV(ostatniaMacierz, i, wynik, true);
                else wynik[i] = WyliczWynikV(ostatniaMacierz, i, wynik);
            }
            return wynik;
        }
        private float WyliczWynikV(Macierze macierz, int id, float[] poprzednieV, bool czyOstatnie = false)
        {
            if (czyOstatnie)
                return macierz.I[id] / macierz.G[id, id];

            float suma = 0f;

            for(int i = id; i < macierz.Id; i++)
            {
                suma += macierz.G[id, i + 1] * poprzednieV[i + 1];
            }
            return (macierz.I[id] - suma) / macierz.G[id, id];
        }
        private float ObliczNoweI(int i, int przeksztalcenie, float[] poprzednieI, float[,] poprzednia)
        {
            var mianownik = poprzednia[przeksztalcenie - 1, przeksztalcenie - 1];
            if (mianownik == 0)
                mianownik = 1;

            return poprzednieI[przeksztalcenie] - ( poprzednieI[przeksztalcenie - 1] * ((poprzednia[i, przeksztalcenie - 1]) / mianownik));
        }
        private float ObliczPrzeksztalcenie(int i, int j, int przeksztalcenie, float[,] poprzednia)
        {
            var pomniejszoneI = i - 1 >= 0 ? i - 1 : 0;
            if (i > this.OrginalG.Length)
                throw new Exception("Za duża ilość przekształcania macierzy");

            var mianownik = poprzednia[przeksztalcenie - 1, przeksztalcenie - 1];
            if (mianownik == 0)
                mianownik = 1;

            return poprzednia[i, j] - ((poprzednia[przeksztalcenie - 1, j]) * ((poprzednia[i, przeksztalcenie - 1]) / mianownik));
        }
        private int ZnajdzIloscPrzeksztalcen(bool czyPierwszeZero = false)
        {
            int wynik = 1;
            if (czyPierwszeZero)
            {
                for (int i = 0; i < this.wilkosc; i++)
                {
                    if (this.OrginalI[i] == 0)
                        return i;
                }
            }
            return wynik;
        }
        private float[] KopiujI(float[] poprzednieI)
        {
            float[] newI = new float[(int)this.wilkosc];
            for(int i = 0; i < this.OrginalI.Count(); i++)
            {
                newI[i] = poprzednieI[i];
            }
            return newI;
        }
        private float[,] KopiujG(float[,] macierzKopiowana)
        {
            float[,] kopia = new float[(int)this.wilkosc , (int)this.wilkosc];
            for(int i = 0; i < this.wilkosc; i++)
            {
                for(int j = 0; j < this.wilkosc; j++)
                {
                    kopia[i, j] = macierzKopiowana[i, j];
                }
            }
            return kopia;
        }
        public class Wynik
        {
            public List<Macierze> WszystkieMacierze { get; set; }
            public float[] V { get; set; }
            public Wynik(List<Macierze> macierze, float[] v)
            {
                this.WszystkieMacierze = macierze;
                this.V = v;
            }
        }
        public class Macierze
        {
            public float[,] G { get; set; }
            public float[] I { get; set; }
            public int Id { get; set; }
            public Macierze(float[,] g, float[] i, int id)
            {
                this.G = g;
                this.I = i;
                this.Id = id;
            }
        }

    }
}
