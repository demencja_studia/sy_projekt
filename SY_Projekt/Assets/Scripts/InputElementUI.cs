﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputElementUI : MonoBehaviour
{
    public Text elementName;
    public Text node_1;
    public Text node_2;
    public Text elementValue;
    public Text elementType;
    
    public void Uzupelnij(string name, int node_1, int node_2, float value, ElementTpe type)
    {
        elementName.text = name;
        this.node_1.text = node_1.ToString();
        this.node_2.text = node_2.ToString();
        elementValue.text = value.ToString();
        elementType.text = type.ToString();
    }

    public void Uzupelnij(InputData data)
    {
        Uzupelnij(data.name, data.node1, data.node2, data.value, data.type);
    }
}
