﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class AddData : MonoBehaviour
{
    public InputField elementName;
    public InputField node_1;
    public InputField node_2;
    public InputField valueNumber;
    public Dropdown type;
    public MainScript mainScript;

    public void Dodaj()
    {
        if (elementName.text == "")
        {
            DodajALL();
            return;
        } else if (elementName.text == "z1")
        {
            DodajZestaw1();
            return;
        } else if(elementName.text == "z2")
        {
            DodajZestaw2();
            return;
        }
        if (this.node_1.text == "") return;
        if (this.node_2.text == "") return;
        if (valueNumber.text == "") return;
        string name = elementName.text;
        int node_1 = int.Parse(this.node_1.text);
        int node_2 = int.Parse(this.node_2.text);
        float value = float.Parse(valueNumber.text, CultureInfo.InvariantCulture.NumberFormat);
        ElementTpe type = (ElementTpe)Enum.Parse(typeof(ElementTpe), "" + this.type.value);
        InputData inputData = new InputData(name, node_1, node_2, value, type);
        mainScript.AddInputData(inputData);
        //elementName.text = "";
        //this.node_1.text = "";
        //this.node_2.text = "";
        //valueNumber.text = "";
        //this.type.value = 0;
    }

    public void DodajALL()
    {
        InputData data1 = new InputData("is1", 0, 1, 1, ElementTpe.SI);
        InputData data2 = new InputData("r1", 1, 2, 10, ElementTpe.R);
        InputData data3 = new InputData("r2", 2, 0, 20, ElementTpe.R);
        InputData data4 = new InputData("r3", 2, 3, 16, ElementTpe.R);
        InputData data5 = new InputData("r4", 3, 0, 4, ElementTpe.R);
        mainScript.AddInputData(data1);
        mainScript.AddInputData(data2);
        mainScript.AddInputData(data3);
        mainScript.AddInputData(data4);
        mainScript.AddInputData(data5);
    }

    public void DodajZestaw1()
    {
        InputData data1 = new InputData("is1", 0, 1, 1, ElementTpe.SI);
        InputData data2 = new InputData("r1", 1, 2, 10, ElementTpe.R);
        InputData data3 = new InputData("r2", 2, 0, 20, ElementTpe.R);
        InputData data4 = new InputData("r3", 2, 3, 16, ElementTpe.R);
        InputData data5 = new InputData("r4", 3, 0, 4, ElementTpe.R);
        InputData data6 = new InputData("E", 3, 0, 1, ElementTpe.Iv);
        mainScript.AddInputData(data1);
        mainScript.AddInputData(data2);
        mainScript.AddInputData(data3);
        mainScript.AddInputData(data4);
        mainScript.AddInputData(data5);
        mainScript.AddInputData(data6);
    }

    public void DodajZestaw2()
    {
        //InputData data1 = new InputData("s1", 0, 2, 1, ElementTpe.SI);
        //InputData data6 = new InputData("E", 3, 0, 1, ElementTpe.Iv);
        //InputData data7 = new InputData("vs", 1, 2, 1, ElementTpe.Iv);
        //mainScript.AddInputData(data1);
        //mainScript.AddInputData(data6);
        //mainScript.AddInputData(data7);
    }
}
