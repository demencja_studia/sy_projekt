﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyMatrix
{
    public float[,] matrixG;
    public float[] matrixI;
    public string[] matrixV;
    private int size;
    private CreatePola pola;

    public MyMatrix(int size, CreatePola pola)
    {
        this.size = size;
        matrixG = new float[size, size];
        matrixI = new float[size];
        matrixV = new string[size];
        this.pola = pola;
        //pola.Create(size);
    }

    private void IncreaseMatrixSize(int increaseToSize)
    {
        //pola.Create(increaseToSize + 1);
        matrixG = KopiujG(matrixG, increaseToSize + 1);
        matrixI = KopiujI(matrixI, increaseToSize + 1);
        matrixV = KopiujV(matrixV, increaseToSize + 1);
    }

    public void Utnij(out float[,] outG, out float[] outI)
    {
        float[] newI = new float[matrixI.Length - 1];
        float[,] newG = new float[matrixG.GetLength(0) - 1, matrixG.GetLength(0) - 1];

        for (int i = 1; i < matrixI.Length; i++)
        {
            newI[i - 1] = matrixI[i];
        }
        //matrixI = newI;
        outI = newI;
        for (int i = 1; i < matrixG.GetLength(0); i++)
        {
            for (int j = 1; j < matrixG.GetLength(0); j++)
            {
                newG[i - 1, j - 1] = matrixG[i, j];
            }
        }
        //matrixG = newG;
        outG = newG;
    }

    private string[] KopiujV(string[] poprzednieI, int nowaWielkosc)
    {
        string[] newV = new string[nowaWielkosc];
        for (int i = 0; i < poprzednieI.Length; i++)
        {
            newV[i] = poprzednieI[i];
        }
        size = nowaWielkosc;
        return newV;
    }
    private float[] KopiujI(float[] poprzednieI, int nowaWielkosc)
    {
        float[] newI = new float[nowaWielkosc];
        for (int i = 0; i < poprzednieI.Length; i++)
        {
            newI[i] = poprzednieI[i];
        }
        size = nowaWielkosc;
        return newI;
    }
    private float[,] KopiujG(float[,] macierzKopiowana, int nowaWielkosc)
    {
        float[,] kopia = new float[nowaWielkosc, nowaWielkosc];
        for (int i = 0; i < macierzKopiowana.GetLength(0); i++)
        {
            for (int j = 0; j < macierzKopiowana.GetLength(0); j++)
            {
                kopia[i, j] = macierzKopiowana[i, j];
            }
        }
        size = nowaWielkosc;
        return kopia;
    }

    public void AddData(InputData data)
    {
        ElementTpe type = data.type;
        switch (type)
        {
            case ElementTpe.SI:
                AddIS(data);
                break;
            case ElementTpe.R:
                AddR(data);
                break;
            case ElementTpe.Iv:
                AddIv(data);
                break;
        }
    }

    private void AddIv(InputData data)
    {
        int maxSize;
        int nPlus;
        int nMinus;

        if (data.node1 > data.node2)
        {
            maxSize = data.node1 + 1;
        }
        else
        {
            maxSize = data.node2 + 1;
        }

        nPlus = data.node1;
        nMinus = data.node2;

        if (matrixG == null || matrixG.GetLength(0) == 0)
        {
            matrixG = new float[maxSize, maxSize];
        }
        if (matrixI == null || matrixI.Length == 0)
        {
            matrixI = new float[maxSize];
        }

        maxSize = matrixI.Length;

        // będzie zawsze za mała
        IncreaseMatrixSize(maxSize);
        float value = data.value * 10f;
        matrixG[maxSize, nPlus] += 1;
        matrixG[maxSize, nMinus] += -1;
        matrixG[nPlus, maxSize] += 1;
        matrixG[nMinus, maxSize] += -1;
        matrixV[maxSize] = "I" + data.name;
        matrixV[nPlus] = "V"+nPlus;
        matrixV[nMinus] = "V"+nMinus;
        matrixI[maxSize] += value;
        MatrixVAddZeroToEmpty();
    }

    private void AddR(InputData data)
    {
        int maxSize;
        int nPlus;
        int nMinus;

        if (data.node1 > data.node2)
        {
            maxSize = data.node1;
        }
        else
        {
            maxSize = data.node2;
        }
        nPlus = data.node1;
        nMinus = data.node2;

        if (matrixG == null)
        {
            matrixG = new float[maxSize, maxSize];
        }
        if (matrixI == null)
        {
            matrixI = new float[maxSize];
        }

        if (matrixG.GetLength(0) > maxSize)
        {
            //odpowiednia wielkosc
            float value = (1f / data.value) * 10f;
            matrixG[nPlus, nPlus] += value;
            matrixG[nMinus, nMinus] += value;
            matrixG[nPlus, nMinus] += -value;
            matrixG[nMinus, nPlus] += -value;
            matrixV[nPlus] = "V" + nPlus;
            matrixV[nMinus] = "V" + nMinus;
            MatrixVAddZeroToEmpty();
        }
        else
        {
            // za mała
            IncreaseMatrixSize(maxSize);
            float value = (1f / data.value) * 10f;
            matrixG[nPlus, nPlus] += value;
            matrixG[nMinus, nMinus] += value;
            matrixG[nPlus, nMinus] += -value;
            matrixG[nMinus, nPlus] += -value;
            matrixV[nPlus] = "V" + nPlus;
            matrixV[nMinus] = "V" + nMinus;
            MatrixVAddZeroToEmpty();
        }
    }

    private void AddIS(InputData data)
    {
        int maxSize;
        int nPlus;
        int nMinus;

        if (data.node1 > data.node2)
        {
            maxSize = data.node1;
        }
        else
        {
            maxSize = data.node2;
        }
        nPlus = data.node1;
        nMinus = data.node2;

        if (matrixG == null)
        {
            matrixG = new float[maxSize, maxSize];
        }
        if (matrixI == null)
        {
            matrixI = new float[maxSize];
        }

        if (matrixI.Length > maxSize)
        {
            //odpowiednia wielkosc
            float value = data.value * 10f;
            matrixI[nPlus] += -value;
            matrixI[nMinus] += value;
            matrixV[nPlus] = "V" + nPlus;
            matrixV[nMinus] = "V" + nMinus;
            MatrixVAddZeroToEmpty();
        }
        else
        {
            // za mała
            IncreaseMatrixSize(maxSize);
            float value = data.value * 10f;
            matrixI[nPlus] += -value;
            matrixI[nMinus] += value;
            matrixV[nPlus] = "V" + nPlus;
            matrixV[nMinus] = "V" + nMinus;
            MatrixVAddZeroToEmpty();
        }
    }

    private void MatrixVAddZeroToEmpty()
    {
        for (int i = 0; i < matrixV.Length; i++)
        {
            if (string.IsNullOrWhiteSpace(matrixV[i]))
            {
                matrixV[i] = "0";
            }
        }
    }

    ///// <summary>
    ///// Przypisuje null do macierzy
    ///// </summary>
    //public void Clear()
    //{
    //    matrixG = new float[size, size];
    //    matrixI = new float[size];
    //}
}
